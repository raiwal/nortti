# Description
QML application to monitor plant status. Possibility to control watering and light.
Shows also some statistics.

Planned layout is in folder hortti_app_qt but integration was delayed to future versions

## Planned
![Planned layout](layout.png)

## Unfinalized 
![Unfinalized layout](another.png)

# Notes 
To execute unit test run following command
```
qmltestrunner -input tst_appname.qml
```

There is still Tests folder for next version c++ unit tests. Those are not tied to this application in anyhow.

# Authors and acknowledgment
Rainer Waltzer @raiwal

Elmo Rohula @rootelmo

pragma Singleton
import QtQuick 2.0

QtObject {
    property color primary_color: "#bababa"
    property color light_color: "#cfcfcf"
    property color dark_color: "#808080"
    property color orange_color: "#ff8b38"
    property color orange_dark: "#9c5800"
}

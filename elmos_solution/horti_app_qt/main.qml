import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import Qt5Compat.GraphicalEffects
import QtQuick.Controls 2.15

Window {
    id: root
    width: 360
    height: 640
    visible: true
    title: qsTr("Horti App")

    color: Style.primary_color

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        Layout.alignment: Qt.AlignHCenter

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 60
            // color: "red"
        }

        Rectangle {
            color: Style.primary_color
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1

            RoundBtn { x: parent.x; y: parent.y / 2 - 35; }
            RoundSlider { x: parent.x + 100; y: parent.y / 2; width_s: parent.width - 100; }
        }

        Rectangle {
            color: Style.primary_color
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1

            RoundSlider { x: parent.x; y: parent.y / 2 - 80}
            RoundBtn { x: parent.x + 250; y: parent.y / 2 - 115; }

        }

        Rectangle {
            id: lastKnobs
            color: Style.primary_color
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 1

            RoundBtn { x: parent.x; y: parent.y / 2 -195; }
            Rectangle {
                RoundSlider { x: parent.x + 100; y: lastKnobs.y / 2 - 185; }
                RoundSlider { x: parent.x + 100; y: lastKnobs.y / 2 - 130; }
            }
        }
    }
}

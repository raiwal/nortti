import QtQuick 2.0
import Qt5Compat.GraphicalEffects

Item {
    Rectangle {
        id: button_base
        height: 75
        width: 75
        radius: height * 0.5

        color: Style.primary_color
        layer.enabled: true

        MouseArea {
            anchors.fill: button_base
            onClicked: push_anim.running = true
        }
    }

    ParallelAnimation {
        id: push_anim
        SequentialAnimation {
            PropertyAnimation{ target: shadow_d; properties: "color"; to: Style.light_color; duration: 25 }
            PropertyAnimation{ target: shadow_d; properties: "color"; to: Style.dark_color; duration: 75 }
        }

        SequentialAnimation {
            PropertyAnimation{ target: shadow_l; properties: "color"; to: Style.dark_color; duration: 25 }
            PropertyAnimation{ target: shadow_l; properties: "color"; to: Style.light_color; duration: 75 }
        }
    }

    DropShadow {
        id: shadow_d
        color: Style.dark_color
        source: button_base
        anchors.fill: button_base
        radius: 7
        horizontalOffset: 3
        verticalOffset: 3
        transparentBorder: true
    }
    DropShadow {
        id: shadow_l
        color: Style.light_color
        anchors.fill: button_base
        source: button_base
        radius: 7
        horizontalOffset: -3
        verticalOffset: -3
        transparentBorder: true
    }
}


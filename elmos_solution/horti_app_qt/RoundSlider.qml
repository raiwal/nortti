import QtQuick 2.0
import Qt5Compat.GraphicalEffects
import QtQuick.Controls 2.15

Item {
    id: slider_item
    property int width_s: 200
    Slider {
        id: slider
        value: 0.5
        width: width_s

        background: Rectangle {
            x: slider.leftPadding
            y: slider.topPadding + slider.availableHegiht * 0.5 - height * 0.5
            implicitWidth: parent.width
            implicitHeight: 10

            width: slider.availableWidth
            height: implicitHeight
            radius: 5
            gradient: Gradient {
                GradientStop { position: -0.2; color: Style.dark_color }
                GradientStop { position: 1.0; color: Style.primary_color }
            }

            // color: root.dark_color
            Rectangle {
                width: slider.visualPosition * parent.width
                        height: parent.height
                        gradient: Gradient {
                            GradientStop { position: -0.2; color: Style.orange_dark }
                            GradientStop { position: 0.7; color: Style.orange_color }
                        }
                        radius: 5
            }
        }
        handle: Rectangle {
            id: handle_rec
            x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
            y: slider.topPadding + slider.availableHeight * 0.5 - 26
            implicitHeight: 32
            implicitWidth: 32
            radius: implicitHeight * 0.5
            color: Style.primary_color
        }
        DropShadow {
            color: Style.dark_color
            source: handle_rec
            anchors.fill: handle_rec
            radius: 3
            horizontalOffset: 2
            verticalOffset: 2
            transparentBorder: true
        }
        DropShadow {
            color: Style.light_color
            anchors.fill: handle_rec
            source: handle_rec
            radius: 3
            horizontalOffset: -2
            verticalOffset: -2
            transparentBorder: true
        }

    }


}

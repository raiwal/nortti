#include <QtTest/QtTest>

class TestQString: public QObject
{
    Q_OBJECT
private slots:
    void toUpper();
    void testGui();
};

void TestQString::toUpper()
{
    QString str = "Hello";
    QCOMPARE(str.toUpper(), QString("HELLO"));
}

void TestQString::testGui()
{
    QCOMPARE(QTest::currentAppName(), "Greenhouse");
}
QTEST_MAIN(TestQString)
#include "testqstring.moc"


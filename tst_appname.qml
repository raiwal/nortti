import QtQuick 2.0
import QtTest 1.2

Item {
    width: 200
    height: 200

    About {
        id:aboutWindow
    }

    TestCase {
        name: "About Window Title test"
        when: windowShown
        function test_title()
        {
            compare(aboutWindow.testing, "About")
        }
    }
}

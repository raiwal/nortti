import QtQuick
import QtQuick.Window

Window {
    id : windowAbout
    width : 200
    height : 200
    visible : true
    color: "grey"

    property string testing: "About"

    title: testing
    Text {
        id: aboutText
        text: "Nothing much about"
    }
}

import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Particles


ApplicationWindow {
    id: root
    width: 360
    height: 560
    visible: true
    title: "Greenhouse"

    property string apptitle: headerName.text
    Loader {
        id : loadAbout
    }

    header: ToolBar {
        RowLayout {
            anchors.fill: parent

            Label {
                id: headerName
                text: "Greenhouse monitoring"
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
            ToolButton {
                text: qsTr("?")
                onClicked: menu.open()
            }

            Menu {
                id:menu
                MenuItem {
                    text: "Help"
                }
                MenuItem {
                    id : about_dialogue
                    text: "About"
                    onClicked: {console.log("About box opened")
                    loadAbout.source = "About.qml"
                    }
                }
            }
        }
    }

    DelegateModel {
        id: statisticsDelegateModel
        model: StatisticsModel { }
        delegate : RowLayout {
            id: statisticsDelegate
            Text { text: name + " : " + percentage }
        }
    }

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent

        GroupBox {
            id: wateringBox
            title: "Watering"
            Layout.fillWidth: true
            RowLayout {
                Dial {
                    id : wateringDial
                    value : 0
                }
                Text {
                    id: wateringValue
                    text: Number(wateringDial.value).toFixed(1) * 100 + " l/h"
                }

                Image {
                    id: tree
                    source: "images/tree.png"
                    ParticleSystem {
                        id: particlesystem
                        x: 0
                        y: 0
                        width: parent.width
                        height: parent.height
                        ImageParticle {
                            source: "images/water.png"
                            groups: ["water"]
                            color: "#2751af"
                            SequentialAnimation on opacity {
                                loops: Animation.Infinite
                                NumberAnimation { from: 0; to: 1; duration: 1000 }
                                NumberAnimation { from: 1; to: 0; duration: 1000 }
                            }
                        }
                        Emitter {
                            group: "water"
                            anchors.fill: parent
                            emitRate: Math.ceil(wateringDial.value*10)
                            lifeSpan: Math.ceil(wateringDial.value) * 5000

                        }
                    }
                }
            }
        }

        GroupBox {
            id : lightTuner
            title: "Light power"
            Layout.fillWidth: true

            RowLayout {
                Dial {
                    id : lightDial
                    value: 0.5
                }
                Text {
                    id: lightValue
                    text: Number(lightDial.value).toFixed(1) * 100 + " %"
                }
                Image {
                    id : light
                    source: "images/light.png"
                    opacity: Number(lightDial.value)
                }
            }
        }

        ListView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: statisticsDelegateModel
            spacing: 10
            leftMargin: 5
        }
    } // RowLayout


}



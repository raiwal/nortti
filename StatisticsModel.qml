import QtQuick 2.0

ListModel {
    id : statisticsModel
    ListElement {
        name: "Humidity"
        percentage: "25 %"
    }
    ListElement {
        name: "Natrium"
        percentage: "0.8 %"
    }
    ListElement {
        name: "Kalium"
        percentage: "3 %"
    }
    ListElement {
        name: "Soil"
        percentage: "Yes"
    }
    ListElement {
        name: "Fan"
        percentage: "Yes"
    }
}
